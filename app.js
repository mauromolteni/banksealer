var conf = require('./config');
var morgan = require('morgan');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var path = require('path');
var fakeDB = require('./src/models/fakeDB');
var realDB = require('./src/models/realDB');
var PORT = conf.web.PORT;
var indexRouter = require('./src/routes/indexRoute');
var authRouter = require('./src/routes/authRoute');

const mongodb_url = 'mongodb://'+ conf.db.username + ':' + conf.db.password + '@' + conf.db.host + conf.db.port + '/' + conf.db.db_name;
console.log(mongodb_url);

if (conf.environment == 'dev') {
  global.myDB = new fakeDB();
} else {
  global.myDB = new realDB(mongodb_url);
}

app.use(express.static('public'));
app.use(express.static('./src/views'));
app.use('/public', express.static(path.resolve(__dirname, 'public')));
app.use('./src', express.static(path.resolve(__dirname, 'src')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use('/api/configurations/', indexRouter);
app.use('/api/auth', authRouter);
app.use(morgan('combined'));

app.use((err, req, res, next) => {
    if (err instanceof SyntaxError) return res.status(400).send(JSON.stringify({
        error: "The body of your request is not valid json!"
    }))

    console.error(err);
    res.status(500).send();
});

app.get('/', function (req, res) {
  res.redirect('index.html');
}); 

var server = app.listen(PORT, function () {
  console.log('Configurations app listening on port ', PORT);
});

module.exports = server;
