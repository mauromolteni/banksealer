var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var config = require('./config');

var jsFiles = ['*.js', 'src/**/*.js'];

gulp.task('default', function(){
    nodemon({
        script:'app.js',
        delayTime: 1,
        ext: 'js',
        env:{
            PORT: 3000
        },
        watch: jsFiles,
        ignore: ['./node_modules']
    }).on('restart', function(){
        console.log('Restarting');
    });
});