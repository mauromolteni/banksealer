HTTP Rest API

This application implements the following API:
- create, read, update and delete some configurations value
- obtain an authentication token

To install you just need to type the following commands:
- npm install
- npm start

You can consume the API through front-end accessing to 
http://localhost:3000/index.html

CRUD CONFIGURATIONS API

The following APIs are available:

- Get all configurations
    GET http://localhost:3000/api/configurations/

- Get a specific configuration
    GET http://localhost:3000/api/configurations/<id>

- Update a configuration
    PUT http://localhost:3000/api/configurations/<id>

- Add a new configuration
    POST http://localhost:3000/api/configurations/<id>

- Delete a configuration
    DELETE http://localhost:3000/api/configurations/<id>

AUTHENTICATION TOKEN
To get an authentication token that will then be spent, call the following API:
POST to http://localhost:3000/api/auth, passing a body
{
    "username": <username>,
    "password": <password>
}

FOLDER STRUCTURE
Inside the main folder you can find the file app.js that is the main entry point of the application and config file which contains some configurations value of the application.

The folder src contains three folders:
- models: it contains the database definitions
- routes: it contains two files that define the routing policy for the existing apis
- views: it contains the front-end files

The folder test contains some test written using mocha: test of the body validation function and test of the apis. To launch you just need to stop the server and type "mocha".
