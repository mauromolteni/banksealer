class FakeDB {

  constructor() {
      this._store = {
        configurations: [
          {
            id:'foo',
            name: 'Configuration for Foo',
            value: 'This is the value for configuration Foo'
          },
          {
              id:'boo',
            name: 'Configuration for Boo',
            value: 'This is the value for configuration Boo'
          }
      ]};
  }

  insert(data) {
    this._store.configurations.push(data);
  };
  
  readData(id=undefined) {
    if (id == undefined){
      return this._store.configurations;
    } else {
      var result = this._store.configurations.find(value => {
        return (value.id === id);
      });
      if (result == undefined) {
          throw new Error('Not found');
      }
      return result;
    }
    
  };
  
  deleteData(id) {
    var storeIndex = this._store.configurations.findIndex(value => {
      return (value.id === id);
    });
    
    if (storeIndex == undefined || storeIndex == -1) {
      throw new Error('Not found');
    }
    var result = this._store.configurations[storeIndex];
    this._store.configurations.splice(storeIndex,1);
    return result;
  };

  updateData(id, new_data) {
    var storeIndex = this._store.configurations.findIndex(value => {
      return (value.id === id);
    });

    if (storeIndex == -1 || storeIndex == undefined) {
      var errorMessage = 'Configuration'.concat(id).concat('not exists');
      throw new Error(errorMessage);
    }

    this._store.configurations[storeIndex].id = new_data.id;
    this._store.configurations[storeIndex].name = new_data.name;
    this._store.configurations[storeIndex].value = new_data.value;
    
    return this._store.configurations[storeIndex];
  };

}

module.exports = FakeDB;
