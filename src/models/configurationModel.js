var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var conf = require('../../config');
var configModel = require('./configurationModel');

var configurationModel = new Schema({
    id: {type: String}, 
    name: {type: String},
    value: {type: String},
});

module.exports = mongoose.model(conf.db.collection, configurationModel);