var mongoose = require('mongoose');
var configModel = require('./configurationModel');
mongoose.Promise = global.Promise;
var assert = require('assert');

class RealDB {

    constructor(url) {
        this._mongodb_url = url;
        this._store = mongoose.connect(this._mongodb_url);
    }

    _read(id=undefined) {
        var prom;
        if (id == undefined){
            prom = configModel.find((err,configs) =>{
                if (err || configs == undefined) {
                    throw new Error('Not found');
                }
            });
        }else {
            prom = configModel.find(id,(err,configs) =>{
                if (err || configs == undefined)
                    throw new Error('Not found');
            });
        }
        return prom.exec();
    }

    readData(id=undefined) {
        var prom = this._read(id);
        prom.then(configs =>{
            console.log('prom');
            configs.forEach(config =>{
                console.log(config);
            })
            return configs;
        }).catch(error => {
            console.log(error);
        })
    };
}

module.exports = RealDB;