var express = require('express');
var router = express.Router();
var moment = require('moment');
var conf = require('../../config');
var SanityCheckResult = require('../../lib/sanitycheckresult');

router.route('/')
.get((req,res)=>{
    console.log(moment.utc().format('YYYY-MM-DD HH:mm:ss') + ' : Received a GET request');
    var result = myDB.readData();
    res.header("Content-Type",'application/json');
    res.status(200);
    res.json({'configurations': result});
});

router.route('/:id')
.get((req,res)=>{
    console.log(moment.utc().format('YYYY-MM-DD HH:mm:ss') + ' : Received a GET request');
    var id = req.params.id;

    try{
        var result = myDB.readData(id);
        res.status(200);
        res.json(result);
    } catch (err) {
        console.error(err);
        res.status(404);
        res.json({Error: 'Configuration ID not exists'});
    }
})
.post((req,res) => {
    console.log(moment.utc().format('YYYY-MM-DD HH:mm:ss') + ' : Received a POST request');
    console.log(req.body);
    var id = req.params.id;
    var result = new SanityCheckResult(req.body);

    if (result.isValid && !existId(id) && id == req.body.id) {
        myDB.insert(req.body);
        res.status(200);
        res.send(req.body);
    }
    else {
        res.status(400);
        var respMsg = {errors:[]};
        for (var reason in result.rejectReasons) {
            respMsg.errors.push(result.rejectReasons);
        }
        res.send(respMsg);
    }
})
.delete((req,res) =>{
    console.log(moment.utc().format('YYYY-MM-DD HH:mm:ss') + ' : Received a DELETE request');
    var id = req.params.id;
    try{
        var result = myDB.deleteData(id);
        res.status(200);
        res.json(result);
    } catch (err) {
        console.error(err);
        res.status(404);
        res.json({Error: 'Configuration ID not exists'});
    }
})
.put((req,res) => {
    console.log(moment.utc().format('YYYY-MM-DD HH:mm:ss') + ' : Received a PUT request');
    var id = req.params.id;
    var result = new SanityCheckResult(req.body);

    if (result.isValid) {
        if(id == req.body.id) {
            try{
                var result = myDB.updateData(id,req.body);
                res.status(200);
                res.json(result);
            } catch (err) {
                console.error(err);
                res.status(404);
                res.json({Error: 'Configuration ID not exists'});
            }
        } else {
            res.status(400);
            res.send();
        }
    } else {
        res.status(400);
        var respMsg = {errors:[]};
        for (var reason in result.rejectReasons) {
            respMsg.errors.push(result.rejectReasons);
        }
        res.send(respMsg);
    }
});

function existId(id) {
    try{
        myDB.readData(id);
        return true;
    } catch (err) {
        return false;
    }
}

module.exports = router;