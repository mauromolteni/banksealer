var express = require('express');
var router = express.Router();
var mongodb = require('mongodb').MongoClient;
var moment = require('moment');
var conf = require('../../config');
var jwtLib = require('jsonwebtoken');

router.route('/')
.post((req,res)=>{
    console.log(moment.utc().format('YYYY-MM-DD HH:mm:ss') + ' : Received an AUTHENTICATION request');
    console.log(req.body);
    var token = jwtLib.sign({
        // expires in one hour
        exp: Math.floor(Date.now() / 1000) + (60 * 60),
        // the payload data arriving from api call
        data: req.body,
      }, 'secret');
    res.send(token);
});

module.exports = router;