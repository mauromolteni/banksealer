var path = require('path');

module.exports = {
    entry: 'app.js',
    environment: 'dev',
    web:
      {
        PORT: 3000
      },
    output: {
        path: path.resolve(__dirname, 'src/views'),
        viewPath: '/src/views',
        publicPath: ''
    },
    db: {
        host: 'ds223019.mlab.com',
        port: 23019,
        db_name: 'banksealer',
        collection: 'Configurations',
        username: 'banksealer-mgmt',
        password: 'password'
    }
  };