var assert = require('chai').assert;
var expect = require('chai').expect;
var request = require('supertest');

const base_url = "/api/configurations";

describe('End 2 End Tests ', function () {
    this.timeout(0);

    before(function(){
        server = require('../app');
    });

    after(function() {
        server.close();
    });


    it('Get all configurations', function (done) {
        request(server)
        .get(base_url+'/')
        .expect(200)
        .end(function(err, res) {
            if (err) throw err;
            assert(res.body.configurations.length === 2, 'Two configurations exist');
            assert(res.body.configurations[0].id === 'foo');
            assert(res.body.configurations[0].name === 'Configuration for Foo');
            assert(res.body.configurations[0].value === 'This is the value for configuration Foo');
            done();
        })
    });

    it('Update a configuration', function (done) {
        const message = {
            id: 'foo',
            name: 'XXXXXX',
            value: 'YYYY'
        };
        request(server)
        .put(base_url+'/foo')
        .type('form')
        .send(message)
        .set('Accept', /application\/json/)
        .expect(200)
        .end(function(err, res) {
            if (err) throw err;
            assert(myDB._store.configurations.length === 2, 'Two configurations exist');
            assert(myDB._store.configurations[0].id === message.id);
            assert(myDB._store.configurations[0].name === message.name);
            assert(myDB._store.configurations[0].value === message.value);
            done();
        })
    });

    it('Delete a configuration', function (done) {
        request(server)
        .delete(base_url+'/foo')
        .expect(200)
        .end(function(err, res) {
            if (err) throw err;
            assert(myDB._store.configurations.length === 1, 'One configuration deleted');
            assert(myDB._store.configurations[0].id !== 'foo');
            assert(res.body.id === 'foo');
            assert(res.body.name === 'XXXXXX');
            assert(res.body.value === 'YYYY');
            done();
        })
    });

    it('New configuration', function (done) {
        const p = {
            id: 'foo',
            name: 'id name',
            value: 'id value'
        }
        request(server)
        .post(base_url+'/foo')
        .type('form')
        .send(p)
        .set('Accept', /application\/json/)
        .expect(200)
        .end(function(err, res) {
            if (err) throw err;
            assert(myDB._store.configurations.length === 2, 'One configuration added');
            assert(myDB._store.configurations[1].id === 'foo');
            assert(myDB._store.configurations[1].name === 'id name');
            assert(myDB._store.configurations[1].value === 'id value');
            assert(res.body.id === 'foo');
            assert(res.body.name === 'id name');
            assert(res.body.value === 'id value');
            done();
        })
    });

});
    
