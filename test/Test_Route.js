var assert = require('chai').assert;
var expect = require('chai').expect;
var request = require('supertest');

const body = {
    "id": "poo",
    "name": "Configuration for Poo",
    "value": "This is the value for configuration Boo"
};

const base_url = "/api/configurations";

describe('Test on route', function () {
    var server;

    before(function(){
        server = require('../app');
    });
    beforeEach(function () {
    });
    afterEach(function () {
    });
    after(function() {
        server.close();
    });

    it('Get /foo, expect 200', function (done) {
        request(server)
        .get(base_url+'/foo')
        .expect(200,done);
    });

    it('Get /pippo, expect 404', function (done) {
        request(server)
        .get(base_url+'/pippo')
        .expect(404,done);
    });

    it('Post /foo, expect 400 already exists', function (done) {
        var p = Object.assign({}, body);
        p.id = 'foo';
        p.name = 'Configuration for Foo';
        p.value = 'This is the value for configuration Foo';
        request(server)
        .post(base_url+'/foo')
        .type('form')
        .send(p)
        .set('Accept', /application\/json/)
        .expect(400,done);                
    });

    it('Post /poo, expect 200', function (done) {
        var p = Object.assign({}, body);
        request(server)
        .post(base_url+'/poo')
        .type('form')
        .send(p)
        .set('Accept', /application\/json/)
        .expect(200,done);  
    });

    it('Post /zoo using a wrong body, expect 400', function (done) {
        var p = Object.assign({}, body);
        p.id = 'zoo';
        p.name = 1;
        delete p.value;
        request(server)
        .post(base_url+'/zoo')
        .type('form')
        .send(p)
        .set('Accept', /application\/json/)
        .expect(400,done);  
    });

    it('Post /zoo using multiple JSON, expect 400', function (done) {
        var p = [];
        var b = Object.assign({}, body);
        p.push(b);
        b.id = 'xxx';
        b.name = 'Configuration for xxx';
        b.value = 'YYYY';
        p.push(b);

        request(server)
        .post(base_url+'/jjj')
        .type('form')
        .send(p)
        .set('Accept', /application\/json/)
        .expect(400,done);  
    });

    it('Get /poo, expect 200', function (done) {
        request(server)
        .get(base_url+'/poo')
        .expect(200,done);
    });

    it('Delete /pippo, expect 404', function (done) {
        request(server)
        .delete(base_url+'/pippo')
        .expect(404,done);
    });

    it('Delete /foo, expect 200', function (done) {
        request(server)
        .delete(base_url+'/foo')
        .expect(200,done);
    });

    it('Put /zorro, expect 404', function (done) {
        var p = Object.assign({}, body);
        p.id = 'zorro';
        p.name = 'Configuration for Zorro';
        p.value = 'This is the value for configuration Zorro';
        request(server)
        .put(base_url+'/zorro')
        .type('form')
        .send(p)
        .set('Accept', /application\/json/)
        .expect(404,done);  
    });

    it('Put /poo, expect 200', function (done) {
        var p = Object.assign({}, body);
        p.id = 'poo';
        p.name = 'XXXXXX';
        p.value = 'YYYY';
        console.log(p);
        request(server)
        .put(base_url+'/poo')
        .type('form')
        .send(p)
        .set('Accept', /application\/json/)
        .expect(200,done);         
    });

    it('Put /foo using wrong body, expect 400', function (done) { // manca layer di verifica del body
        var p = Object.assign({}, body);
        p.id = 'foo';
        delete p.name;
        delete p.value;
        p.test = 'XXXX';
        request(server)
        .put(base_url+'/foo')
        .type('form')
        .send(p)
        .set('Accept', /application\/json/)
        .expect(400,done); 
    });
});