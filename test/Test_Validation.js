var SanityCheckResult = require('../lib/sanitycheckresult');
var assert = require('chai').assert;
var expect = require('chai').expect;

const body = {
    id: 'poo',
    name: 'Configuration for Poo',
    value: 'This is the value for configuration Boo'
};

describe('Body Validation', function () {
    var mybody;
    beforeEach(function () {
        mybody = Object.assign({}, body);
    });

    it('Should Accept', function () {
        var packet = JSON.parse(JSON.stringify(mybody));
        var r = new SanityCheckResult(packet);
        assert(r.isValid);
        assert.equal(r.rejectReasons.length, 0);
    });

    it('Should Reject - Id is missing', function () {
        delete mybody.id;
        var packet = JSON.parse(JSON.stringify(mybody));
        var r = new SanityCheckResult(packet);
        assert(!r.isValid);
        assert.equal(r.rejectReasons.length, 1);
        assert.equal(r.rejectReasons[0], 'Id is missing');
    });

    it('Should Reject - Name is missing', function () {
        delete mybody.name;
        var packet = JSON.parse(JSON.stringify(mybody));
        var r = new SanityCheckResult(packet);
        assert(!r.isValid);
        assert.equal(r.rejectReasons.length, 1);
        assert.equal(r.rejectReasons[0], 'Name is missing');
    });

    it('Should Reject - Value is missing', function () {
        delete mybody.value;
        var packet = JSON.parse(JSON.stringify(mybody));
        var r = new SanityCheckResult(packet);
        assert(!r.isValid);
        assert.equal(r.rejectReasons.length, 1);
        assert.equal(r.rejectReasons[0], 'Value is missing');
    });

    it('Should Reject - Id and value are missing', function () {
        delete mybody.id;
        delete mybody.value;
        var packet = JSON.parse(JSON.stringify(mybody));
        var r = new SanityCheckResult(packet);
        assert(!r.isValid);
        assert.equal(r.rejectReasons.length, 2);
        assert.equal(r.rejectReasons[0], 'Id is missing');
        assert.equal(r.rejectReasons[1], 'Value is missing');
    });
});