class SanityCheckResult {
    /**
     * Creates an instance of SanityCheckResult.
     * @param {String|Object} rawPacket 
     * @memberof SanityCheckResult
     */
    constructor(rawPacket) {
        this.rejectReasons = [];
        if (typeof rawPacket == 'string') {
            try {
                this.packet = JSON.parse(rawPacket.toString('utf8'));
            } catch (err) {
                this.packet = rawPacket.toString('utf8');
                this.reject(`Invalid JSON ${err.message}`);
            }
        } else if (typeof rawPacket == 'object') {
            this.packet = rawPacket;
            if (this.packet == null) {
                this.reject('Null JSON');
            }
        } else {
            if (rawPacket != null) {
                this.packet = rawPacket.toString();
            } else {
                this.packet = null;
            }
            this.reject('Unspported input type');
        }
        this.validation(this.packet);
    }

    validation(packet){
        if (!packet.id) {
            this.reject('Id is missing');
        }

        if (packet.name) {
            if (typeof(packet.name) != 'string') {
                this.reject('Name is not a string');
            }
        }
        else {
            this.reject('Name is missing');
        }

        if (!packet.value) {
            this.reject('Value is missing');
        }
    }

    get isValid() {
        return this.rejectReasons.length == 0;
    }

    reject(message) {
        this.rejectReasons.push(message);
    }
}


module.exports = SanityCheckResult;