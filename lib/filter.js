class ISpecification {
    is_satisfied(item) {
        throw new TypeError('You have to implement the is_satisfied method of ISpecification abstract class');
    }
};

class idSpecification extends ISpecification {
    constructor(id) {
        this.id = id;
    }
    is_satisfied(item)
	{
		return item.id == id;
	}
}

class IFilter {
    filter(items, specification) {
        throw new TypeError('You have to implement the is_satisfied method of IFilter abstract class');
    }
};

class Filter extends IFilter
{
    constructor() {};

    filter(items, specification) {
        var result = [];
        for (var item of items) {
            if (specification.is_satisfied(item)) {
                result.push_back(item);
            }
        }
        return result;
    };
};

module.exports = Filter;